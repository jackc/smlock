# smlock makefile
VERSION = 0.7.5
# paths
PREFIX = /usr/local
CFLAGS = -pedantic -Wall -Os -I. -I/usr/include -DVERSION=\"${VERSION}\" -DHAVE_SHADOW_H
LDFLAGS = -s -L/usr/lib -lc -lcrypt -lX11 -lXext
# On *BSD remove -DHAVE_SHADOW_H from CPPFLAGS and add -DHAVE_BSD_AUTH
# On OpenBSD and Darwin remove -lcrypt from LIBS
CC = cc
# Install mode. On BSD systems MODE=2755 and GROUP=auth, on others MODE=4755 and GROUP=root
#MODE=2755
#GROUP=auth

all: options smlock smlocki

options:
	@echo smlock build options:
	@echo "CFLAGS   = ${CFLAGS}"
	@echo "LDFLAGS  = ${LDFLAGS}"
	@echo "CC       = ${CC}"

.c.o:
	@echo CC $<
	@${CC} -c ${CFLAGS} $<

smlock.o:

smlock: smlock.o
	@echo CC -o $@
	@${CC} -o $@ smlock.o ${LDFLAGS}

smlocki: smlocki.o
	@echo CC -o $@
	@${CC} -o $@ smlocki.o ${LDFLAGS} -lImlib2

clean:
	@echo cleaning
	@rm -f smlock smlocki smlock.o smlocki.o smlock-${VERSION}.tar.gz

dist: clean
	@echo creating dist tarball
	@mkdir -p smlock-${VERSION}
	@cp -R LICENSE Makefile README config.mk smlock.c smlock-${VERSION}
	@tar -cf smlock-${VERSION}.tar smlock-${VERSION}
	@gzip smlock-${VERSION}.tar
	@rm -rf smlock-${VERSION}

install: all
	@echo installing executable file to ${DESTDIR}${PREFIX}/bin
	@mkdir -p ${DESTDIR}${PREFIX}/bin
	@cp -f smlock ${DESTDIR}${PREFIX}/bin
	@chmod 755 ${DESTDIR}${PREFIX}/bin/smlock
	@chmod u+s ${DESTDIR}${PREFIX}/bin/smlock
	@cp -f smlocki ${DESTDIR}${PREFIX}/bin
	@chmod 755 ${DESTDIR}${PREFIX}/bin/smlocki
	@chmod u+s ${DESTDIR}${PREFIX}/bin/smlocki

uninstall:
	@echo removing executable file from ${DESTDIR}${PREFIX}/bin
	@rm -f ${DESTDIR}${PREFIX}/bin/smlock
	@rm -f ${DESTDIR}${PREFIX}/bin/smlocki

.PHONY: all options clean dist install uninstall
