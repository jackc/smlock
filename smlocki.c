// smlock 0.7.5
#define _XOPEN_SOURCE 500
#if HAVE_SHADOW_H
#include <shadow.h>
#endif
#include <ctype.h>
#include <pwd.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <crypt.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <linux/vt.h>
#include <fcntl.h> 
#include <errno.h> 
#include <termios.h>
#include <X11/keysym.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/extensions/dpms.h>
#include <time.h>
#include <Imlib2.h>
#if HAVE_BSD_AUTH
#include <login_cap.h>
#include <bsd_auth.h>
#endif


static void end(const char *errstr, ...) {
    va_list ap;
    va_start(ap, errstr);
    vfprintf(stderr, errstr, ap);
    va_end(ap);
    exit(EXIT_FAILURE);
}

#ifndef HAVE_BSD_AUTH
static const char * get_password() { /* only run as root */
    const char *rval;
    struct passwd *pw;
    if(geteuid() != 0) end("cannot retrieve password entry (make sure to suid smlock)\n");
    pw = getpwuid(getuid());
    endpwent();
    rval = pw->pw_passwd;

#if HAVE_SHADOW_H
    {
        struct spwd *sp;
        sp = getspnam(getenv("USER"));
        endspent();
        rval = sp->sp_pwdp;
    }
#endif
    /* drop privileges temporarily */
    if (setreuid(0, pw->pw_uid) == -1) end("cannot drop privileges\n");
    return rval;
}
#endif

int main(int argc, char **argv) {

#ifndef HAVE_BSD_AUTH
    const char *pws;
#endif
    Cursor invisible;
    Display *dpy;
    KeySym ksym;
    Pixmap pmap, pix;
    Window root, w;
    XColor black, dummy, fgcolor, bgcolor;
    XEvent ev;
    XSetWindowAttributes wa;
    XFontStruct *font, *fontt, *fontd;
    GC gc; 
    XGCValues values;
    XCharStruct overall;
    char curs[] = {0, 0, 0, 0, 0, 0, 0, 0};
    short depth, pbl = 256;
    char buf[32], passwd[pbl], passd[pbl];
    int num, len, screen, term, pid, x, dir, ascent, descent;
	unsigned int ypost, yposs, xposs, blen = 0, cs = 0, iw = 0, ih = 0, width = 0, height = 0;
	bool running = 1, update = 1, sleepmode = 0, txtc = 0, showl = 1, sldate = 0, icmc = 0, ecmc = 0, fgc = 0, bgc = 0, ic = 0;
	char dfc = 0, pchar = '*';
	char *tfor = "%R", *banner = "", *tfors = NULL, *fgcstr = NULL, *bgcstr = NULL, *icmd = NULL, *ecmd = NULL;
    char *fontstr = "-*-dejavu sans light-*-r-*--*-400-*-*-*-*-*-*";
    char *fontstrt = "-*-dejavu sans light-*-r-*--*-1050-*-*-*-*-*-*";
    char pat[] = "/opt/smlockbg";
    char *fontstrd = fontstr;
    char erf[20] = "font string error!\n";
    char erd[20] = "date format error!\n";
    char erc[21] = "color format error!\n";
    char nnt[16], snt[16];


    for (int i = 0; i < argc; ++i) {
        if (!strcmp(argv[i], "-t")) {
                if (i + 1 < argc) {
					if ( strlen(argv[++i]) < 17 ) {
                    tfor = argv[i]; } }
                else end(erd);
        }
        else if (!strcmp(argv[i], "-T")) {
                if (i + 1 < argc) {
					sldate = 1;
					if ( strlen(argv[++i]) < 17 ) {
                    tfors = argv[i]; } }
                else end(erd);
        }
        else if (!strcmp(argv[i], "-f")) {
                if (i + 1 < argc) {
					if ( strlen(argv[++i]) < 99 ) {
						fontstr = argv[i]; } }
                else end("font 1 string error.\n");
        }
        else if (!strcmp(argv[i], "-F")) {
                if (i + 1 < argc) {
					if ( strlen(argv[++i]) < 99 ) {
                    fontstrt = argv[i]; } }
                else end("font 2 string error.\n");
        }
        else if (!strcmp(argv[i], "-d")) { 
                if (i + 1 < argc) {
					if ( (cs = strlen(argv[++i])) < 99 ) {
					if ( cs == 1 ) {
						if ( argv[i][0] == '1' ) { dfc = 1; } // font
						else if ( argv[i][0] == '2' ) { dfc = 2; } } // fontt
					    else { dfc = 3; // fontd
                        fontstrd = argv[i]; } } }
                else end("font 3 string error.\n");
        }
        else if (!strcmp(argv[i], "-m")) {
				if (i + 1 < argc) {
					if ( (blen = strlen(argv[++i])) < 99 ) {
					txtc = 1;
					banner = argv[i]; } }
                else end("text string error.\n");
        }
        else if (!strcmp(argv[i], "-c")) {
                if (i + 1 < argc) {
                    if ( (cs = strlen(argv[++i])) < 55 ) {
					icmd = malloc(cs+3);
					icmc = 1;
                    strcpy( icmd, argv[i] );
                    strcat( icmd, " &" ); } }
                else end("initial command string error.\n");
        }
        else if (!strcmp(argv[i], "-C")) {
                if (i + 1 < argc) {
                    if ( (cs = strlen(argv[++i])) < 55 ) {
					ecmd = malloc(cs+3);
					ecmc = 1;
                    strcpy( ecmd, argv[i] );
                    strcat( ecmd, " &"); } }
                else end("final command string error.\n");
        }
        else if (!strcmp(argv[i], "-o")) {
                if (i + 1 < argc) {
                    if ( strlen(argv[++i]) < 33 ) {
					fgc = 1;
                    fgcstr = argv[i]; } }
                else end(erc);
        }
        else if (!strcmp(argv[i], "-O")) {
                if (i + 1 < argc) {
                    if ( strlen(argv[++i]) < 33 ) {
					bgc = 1;
                    bgcstr = argv[i]; } }
                else end(erc);
        }
        else if (!strcmp(argv[i], "-h")) showl = 0;
        else if (!strcmp(argv[i], "-?")) {
            end("\nsmlock 0.7\n\nOPTIONS:\n-m	message\n-f	message font\n-F	clock font\n-d	date font: 1, 2 or font\n-o	text color\n-O	background color\n-t	clock format\n-T	date format\n-h	hide line\n-c	command at startup\n-C	command on exit\n\nBackground image: smlocki will use the picture /opt/smlockbg, so copy and rename a pic. The image will not be resized.\n\nFont: font string in the X Logical Font Description format; the default font is Dejavu. The font must be in the xorg font path. Use xfontsel to find.\n\nTime, date: the time or date format for the strftime function, example: \"%%I:%%M%%P\".\n\nColors: the color for the function XParseColor. It support multiple formats and names, example: \"rgb:ff/ff/ff\"\n\n");
            exit(0);
        }
    }
    if ( access(pat, R_OK ) == 0 ) ic = 1;

    // fill with password characters
        for (int i = 0; i < pbl; ++i) passd[i] = pchar;
    /* disable tty switching */
    if ((term = open("/dev/console", O_RDWR)) == -1) {
        perror("error opening console");
    }
    if ((ioctl(term, VT_LOCKSWITCH)) == -1) {
        perror("error locking console"); 
    }
    /* initial command */
    if (icmc) {
       system(icmd);
       free(icmd);
    }
    /* deamonize */
    pid = fork();
    if (pid < 0) 
        end("Could not fork smlock.");
    if (pid > 0) 
        exit(0); // exit parent 
#ifndef HAVE_BSD_AUTH
    pws = get_password();
#endif

    if(!(dpy = XOpenDisplay(0))) end("cannot open dpy\n");
    screen = DefaultScreen(dpy);
    root = RootWindow(dpy, screen);
    width = DisplayWidth(dpy, screen);
    height = DisplayHeight(dpy, screen);
    Colormap cm = DefaultColormap(dpy, screen);
    depth = DefaultDepth(dpy, screen);
    Visual *vi = DefaultVisual(dpy, screen);
    Imlib_Image img;
    wa.override_redirect = 1;
	if (ic) { img = imlib_load_image(pat); // background load
	if (img) { bgc = 0; }
	else { ic = 0; printf("error loading background!\n"); } }
    XAllocNamedColor(dpy, cm, "black", &black, &dummy);
	if (bgc) {
	cs = XParseColor(dpy, cm, bgcstr, &bgcolor); //"rgb:50/ff/80"
	if (!cs) end(erc);
	XAllocColor(dpy, cm, &bgcolor);
	wa.background_pixel = bgcolor.pixel; }
	else wa.background_pixel = BlackPixel(dpy, screen);
    if (fgc) {
	cs = XParseColor(dpy, cm, fgcstr, &fgcolor); //"rgb:50/ff/80"
	if (!cs) end(erc);
	XAllocColor(dpy, cm, &fgcolor); }
    w = XCreateWindow(dpy, root, 0, 0, width, height, 0, depth, CopyFromParent, DefaultVisual(dpy, screen), CWOverrideRedirect | CWBackPixel, &wa);
    gc = XCreateGC(dpy, w, (unsigned long)0, &values);
    // background
	if (ic) {
	imlib_context_set_image(img);
    iw = imlib_image_get_width();
    ih = imlib_image_get_height();
    pix = XCreatePixmap(dpy, w, iw, ih, depth);
    imlib_context_set_display(dpy);
    imlib_context_set_visual(vi);
    imlib_context_set_colormap(cm);
    imlib_context_set_drawable(pix);
    imlib_render_image_on_drawable(0,0);
    XSetWindowBackgroundPixmap(dpy, w, pix);
    imlib_free_image(); }
    pmap = XCreateBitmapFromData(dpy, w, curs, 8, 8);
    invisible = XCreatePixmapCursor(dpy, pmap, pmap, &black, &black, 0, 0);
    XDefineCursor(dpy, w, invisible);
    XMapRaised(dpy, w);

    font = XLoadQueryFont(dpy, fontstr);
    fontt = XLoadQueryFont(dpy, fontstrt);
    if (font == 0 || fontt == 0 ) { end(erf); }
    bool lsb = 1, sid = 0, lfa = 0; // font
    fontd = font;
    if ( sldate && dfc ) {
		switch(dfc) {
            case 2: fontd = fontt; lsb = 0; lfa = 1; break; // fontt
            case 3: // fontd
            if ( (fontd = XLoadQueryFont(dpy, fontstrd)) == 0 ) { end(erf); }
            else { sid = 1; lfa = 1; }
            break; } } // font

    time_t nd = time(0);
    cs = strftime(nnt, 16, tfor, localtime(&nd));
    if (!cs) end(erd);
    short tlens = 0, tlen = strlen(nnt);
    if ( sldate ) {
	cs = strftime(snt, 16, tfors, localtime(&nd));
	if (!cs) end(erd);
	tlens = strlen(snt); }
    short vsp = (height / 100) * 2; // space between lines
    unsigned int linexs = width * 3 / 8; // line size and pos
    unsigned int linexe = width * 5 / 8;
    unsigned int liney = height / 2;
    XSetFont(dpy, gc, font->fid); // text size and pos
    XTextExtents(font, banner, blen, &dir, &ascent, &descent, &overall);
	unsigned int xposb = (width - XTextWidth(font, banner, blen)) / 2;
    unsigned int yposb = liney - vsp;
    unsigned int yposp = liney + vsp + ascent; // password y pos
    if ( sldate )  { // date size and pos
	if ( dfc > 1 ) XTextExtents(fontd, banner, blen, &dir, &ascent, &descent, &overall);
	yposs = ( height / 4 ) + ( vsp / 2 ) + ascent;
	xposs = ( width - XTextWidth(fontd, snt, tlens) ) / 2; }
    XSetFont(dpy, gc, fontt->fid); // clock size and pos
    XTextExtents(fontt, nnt, 1, &dir, &ascent, &descent, &overall);
    unsigned int xpost = (width - XTextWidth(fontt, nnt, tlen)) / 2;
    if ( sldate )  ypost = ( height / 4 ) - ( vsp / 2 );
    else ypost = ( height / 4 ) + ( ascent / 2 );
    if ( fgc ) XSetForeground(dpy, gc, fgcolor.pixel);
    else XSetForeground(dpy, gc, WhitePixel(dpy, screen));

    if ((running = running)) {
        for (len = 1000; len; --len) {
            if (XGrabKeyboard(dpy, root, True, GrabModeAsync, GrabModeAsync, CurrentTime) == GrabSuccess) break;
            usleep(1000);
        }
        running = (len > 0); }
    len = 0;
    XSync(dpy, False);


    /* main event loop */
    while(running && !XNextEvent(dpy, &ev)) {
        if (sleepmode) {
            DPMSEnable(dpy);
            DPMSForceLevel(dpy, DPMSModeOff);
            XFlush(dpy); }
           
        if (update) {
            nd = time(0);
            strftime(nnt, 16, tfor, localtime(&nd));
            XTextExtents(font, passd, len, &dir, &ascent, &descent, &overall);
            x = (width - overall.width) / 2;
            XSetFont(dpy, gc, fontt->fid);
            XClearWindow(dpy, w);
            XDrawString(dpy,w,gc, xpost, ypost, nnt, tlen); // clock            
            if (sldate) {
			if ( lsb ) { if ( sid ) { XSetFont(dpy, gc, fontd->fid); }
			else { XSetFont(dpy, gc, font->fid); } }
			XDrawString(dpy,w,gc, xposs, yposs, snt, tlens); // date
			if ( lfa ) { XSetFont(dpy, gc, font->fid); } }
			else XSetFont(dpy, gc, font->fid);
            if (txtc) { XDrawString(dpy,w,gc, xposb, yposb, banner, blen); // text
            if (showl) { XDrawLine(dpy, w, gc, linexs, liney, linexe, liney); } } // line
            XDrawString(dpy,w,gc, x, yposp, passd, len); // password
            update = False;
        }
        if (ev.type == KeyPress) {
            sleepmode = False;
            buf[0] = 0;
            num = XLookupString(&ev.xkey, buf, sizeof buf, &ksym, 0);
            if (IsKeypadKey(ksym)) {
                if (ksym == XK_KP_Enter) ksym = XK_Return;
                else if (ksym >= XK_KP_0 && ksym <= XK_KP_9) ksym = (ksym - XK_KP_0) + XK_0;
            }
            if (IsFunctionKey(ksym) || IsKeypadKey(ksym) || IsMiscFunctionKey(ksym) || IsPFKey(ksym) || IsPrivateKeypadKey(ksym))
				continue;

            switch(ksym) {
                case XK_Return:
                    passwd[len] = 0;
#ifdef HAVE_BSD_AUTH
                    running = !auth_userokay(getlogin(), NULL, "auth-xlock", passwd);
#else
                    running = strcmp(crypt(passwd, pws), pws);
#endif
                    len = 0;
                    break;
                case XK_Escape:
                    len = 0;
                    if (DPMSCapable(dpy)) {
                        sleepmode = True;
                    }
                    break;
                case XK_BackSpace:
                    if (len)
                        --len;
                    break;
                default:
                    if (num && !iscntrl((int) buf[0]) && (len + num < sizeof passwd)) { 
                        memcpy(passwd + len, buf, num);
                        len += num;
                    }
                    break;
            }
            update = True; // show changes
        }
    }

    /* free and unlock */
    setreuid(geteuid(), 0);
    if ((ioctl(term, VT_UNLOCKSWITCH)) == -1) {
        perror("error unlocking console"); }
    close(term);
    setuid(getuid()); // drop rights permanently
	/* final command */
    if (ecmc) {       
    system(ecmd);
    free(ecmd);
    }
    XFreePixmap(dpy, pmap);
    XFreeFont(dpy, font);
    XFreeFont(dpy, fontt);
    XFreeGC(dpy, gc);
    XDestroyWindow(dpy, w);
    XCloseDisplay(dpy);
    return 0;
}
