# smlock
Smlock is a minimal screen locker (Xorg) optimized for mobile.  
Fork of Benruijl sflock - github.com/benruijl/sflock  
It shows a clock and optionally the date and a message. You can customize font, size, colors and the format.  
Use smlocki for the background image.  
## Install
REQUIRES xlib and imlib2. The Dejavu font is the default.  
On Arch:  
`sudo pacman -S xlib11 imlib2 ttf-dejavu`  
On Debian, Ubuntu:  
`sudo apt install libxtst-dev libimlib2 fonts-dejavu`  
Build and install:  
`sudo make clean install`  
## Options
\-m	message  
\-f	message font  
\-F	clock font  
\-d	date font: 1, 2 or font  
\-o	text color  
\-O	background color  
\-t	clock format  
\-T	date format  
\-h	hide line  
\-c	command at startup  
\-C	command on exit  

Background image: Use smlocki. The picture at /opt/smlockbg will be used, so copy and rename a picture. The image will not be resized.

Font: font string in the X Logical Font Description format; the default font is Dejavu. The font must be in the xorg font path. Use xfontsel to make the string.

Time and date: the time or date format for the c strftime function, example: "%I:%M%P".

Colors: the color for the function XParseColor. It support multiple formats and names, example: "rgb:ff/ff/ff" 

Examples:  
`smlock -h -i onboard`  
